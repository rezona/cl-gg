package gg.cl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * The main class which initialises and starts the server.
 * @author Azhar
 * Date: 29 May 2018, 14:46:10
 */
@SpringBootApplication
public class CLGG extends SpringBootServletInitializer {

    /**
     * The entry point of the application.
     * @param args The command line arguments.
     */
    public static void main(String[] args) {
        SpringApplication.run(CLGG.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CLGG.class);
    }

}